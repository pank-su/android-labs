package com.example.task2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        Button btn = (Button) view;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        String text = btn.getText().toString();
        Fragment fragment;
        switch (text) {
            case "b1":
                fragment = new FirstFragment();
                break;
            case "b2":
                fragment = FragmentGeneric.getInstance(0x908884ff, "Second");
                break;
            case "b3":
                fragment = FragmentGeneric.getInstance(0x90D7BCE8, "Third");
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + text);
        }
        fragmentTransaction.replace(R.id.host, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


}